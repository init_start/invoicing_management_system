import request from "./request"

const api = {}
api.login = function(data){
    return request.post("/api/userLogin",data);
}

api.getSysName = ()=>{
    return request.get('/api/sysName');
}

export default api
