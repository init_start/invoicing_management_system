var i18n = function(options){
    var i = new Object();
    i.languageData = {},
    i.options = {
        lang: "",
        defaultLang: "",
        filePath: "/i18n/",
        filePrefix: "i18n_",
        fileSuffix: "",
        forever: true,
        callback: function() {}
    };
    i.transfer = (key)=>{
        return Object.keys(i.languageData).length > 0 && i.languageData[key];
    };
    i.getCookie = function(name) {
        var arr = document.cookie.split('; ');
        for (var i = 0; i < arr.length; i++) {
            var arr1 = arr[i].split('=');
            if (arr1[0] == name) {
                return arr1[1];
            }
        }
        return '';
    };
    i.removeCookie = function(name){

    };
    i.setCookie = function(name, value, myDay) {
        var oDate = new Date();
        oDate.setDate(oDate.getDate() + myDay);
        document.cookie = name + '=' + value + '; expires=' + oDate;
    };
    i.options = options;
    if (i.getCookie('i18n_lang') != "" && i.getCookie('i18n_lang') != "undefined" && i.getCookie('i18n_lang') != null) {
        i.options.defaultLang = i.getCookie('i18n_lang');
    } else if (i.options.lang == "" && i.options.defaultLang == "") {
        throw "defaultLang must not be null !";
    };

    if (i.options.lang != null && i.options.lang != "") {
        if (i.options.forever) {
            i.setCookie('i18n_lang', i.options.lang);
        } else {
            i.removeCookie("i18n_lang");
        }
    } else {
        i.options.lang = i.options.defaultLang;
    }
    $.getJSON(i.options.filePath + i.options.filePrefix + i.options.lang + i.options.fileSuffix + ".json", function(data) {
        var i18nLang = {};
        if (data != null) {
            i18nLang = data;
            i.languageData = data;
        }
        $('[i18n]').each(function(i) {
            var i18nOnly = $(this).attr("i18n-only");
            if ($(this).val() != null && $(this).val() != "") {
                if (i18nOnly == null || i18nOnly == undefined || i18nOnly == "" || i18nOnly == "value") {
                    $(this).val(i18nLang[$(this).attr("i18n")])
                }
            }
            if ($(this).html() != null && $(this).html() != "") {
                if (i18nOnly == null || i18nOnly == undefined || i18nOnly == "" || i18nOnly == "html") {
                    $(this).html(i18nLang[$(this).attr("i18n")])
                }
            }
            if ($(this).attr('placeholder') != null && $(this).attr('placeholder') != "") {
                if (i18nOnly == null || i18nOnly == undefined || i18nOnly == "" || i18nOnly == "placeholder") {
                    $(this).attr('placeholder', i18nLang[$(this).attr("i18n")])
                }
            }
        });
        i.options.callback();
    });
    return i;
}