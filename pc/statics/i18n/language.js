function select(){
    $(document).click(function(){
        $(".select_module_con ul").slideUp();
    })
    var module=$(".select_result");
    module.click(function(e){
        e.stopPropagation();
        var ul=$(this).next();
        ul.stop().slideToggle();
        ul.children().click(function(e){
            e.stopPropagation();
            $(this).parent().prev().children("span").text($(this).text());
            ul.stop().slideUp();
            var condition = $(this).text();  //根据按钮显示  中 文/English
            if(condition == 'English'){
                console.log(condition)
                defaultLang = "en",
                    languageSelect(defaultLang);
            }else{
                defaultLang = "cn",
                    languageSelect(defaultLang);
            }
        })
    })
}
$(function(){
    select();
    languageSelect(defaultLang);
});
/*默认语言*/
var defaultLang = "cn"
var I18n;
function languageSelect(defaultLang){
    I18n = i18n({
        defaultLang: defaultLang,
        filePath: "/statics/data/",
        filePrefix: "i18n_",
        fileSuffix: "",
        forever: true,
        callback: function(res) {}
    });
}
window.onload = function(){

}
